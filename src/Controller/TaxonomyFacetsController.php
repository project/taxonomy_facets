<?php

namespace Drupal\taxonomy_facets\Controller;

use Drupal\node\Entity\Node;
use Drupal\taxonomy_facets\GetNodes;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class TaxonomyFacetsController extends ControllerBase {

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->pagerManager = $container->get('pager.manager');
    $instance->config = $container->get('config.factory')->get('taxonomy_facets.settings');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function content($taxo_facets_path): array {
    $show_nodes_if_no_filters = $this->config->get('show_nodes_if_no_filters');

    if ($show_nodes_if_no_filters || $taxo_facets_path !== 'no-argument') {

      $selected_filters = taxonomy_facets_get_selected_filters($taxo_facets_path);
      $filters = [$selected_filters->getAppliedFilterTids()];

      $filters = current($filters);
      $getNodes = new GetNodes($filters);
      $nodes = $getNodes->getNodes();

      $num_per_page = $this->config->get('number_of_nodes_per_page');
      $this->pagerManager->createPager(count($nodes), $num_per_page);

      if ($nodes = Node::loadMultiple($nodes)) {
        $output = $this->entityTypeManager()->getViewBuilder('node')->viewMultiple($nodes);
        $output[] = ['#type' => 'pager'];
      }
      else {
        $output['no_content'] = [
          '#prefix' => '<p>',
          '#markup' => t('There is currently no content classified with this combination of filters. Try removing one or more filters'),
          '#suffix' => '</p>',
        ];
      }
    }
    else {
      $output['no_content'] = [
        '#prefix' => '<p>',
        '#markup' => t('Please select one or more filters'),
        '#suffix' => '</p>',
      ];
    }
    return $output;
  }

  /**
   * Returns a page title.
   */
  public function getTitle() {
    return $this->config->get('page_title');
  }

}
