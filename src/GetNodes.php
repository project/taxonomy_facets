<?php

namespace Drupal\taxonomy_facets;

/**
 * Class devoted to work with nodes.
 */
class GetNodes {

  /**
   * Allowed filters.
   *
   * @var mixed
   */
  private $filters;

  /**
   * Allowed node types.
   *
   * @var array|null
   */
  private $nodeTypes = NULL;

  /**
   * Starting number.
   *
   * @var float|int
   */
  private $start;

  /**
   * Number of nodes per page.
   *
   * @var array|mixed|null
   */
  private $limit;

  /**
   * Number of nodes.
   *
   * @var mixed
   */
  private $numberOfNodes;

  /**
   * Constructor for the GetNodes class.
   */
  public function __construct($filters) {

    $this->filters = $filters;

    $config = \Drupal::config('taxonomy_facets.settings');
    $this->limit = $config->get('number_of_nodes_per_page');

    // Set node type that are part of filtering, from user config.
    $contentTypes = \Drupal::service('entity_type.manager')->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      if ($config->get('ct_' . $contentType->id())) {
        $this->nodeTypes[] = $contentType->id();
      }
    }

    if (empty($_REQUEST['page'])) {
      $this->start = 0;
    }
    else {
      $this->start = $_REQUEST['page'] * $this->limit;
    }
  }

  /**
   * Gets nodes.
   */
  public function getNodes() {

    $query = \Drupal::database()->select('node', 'n')
      ->fields('n', ['nid']);

    $cnt = 0;
    foreach ($this->filters as $key => $value) {
      $query->innerJoin('taxonomy_index', 'ti' . $cnt, 'n.nid = ti' . $cnt . ' .nid ');
      $query->condition('ti' . $cnt . '.tid', $value);
      $cnt++;
    }
    if ($this->nodeTypes) {
      $query->condition('n.type', $this->nodeTypes, 'IN');
    }

    // Count all nodes first.
    $number_of_nodes = $query->execute();
    $number_of_nodes->allowRowCount = TRUE;
    $this->numberOfNodes = $number_of_nodes->rowCount();

    // Add range, for use in pager.
    $query->range($this->start, $this->limit);
    $nodes = $query->execute()->fetchAll();
    $return = [];
    foreach ($nodes as $node) {
      $return[] = $node->nid;
    }
    return $return;
  }

  /**
   * Gets the number of nodes.
   */
  public function getNumberOfNodes() {
    return $this->numberOfNodes;
  }

}
