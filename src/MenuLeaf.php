<?php

namespace Drupal\taxonomy_facets;

/**
 * Class devoted to work with menu leaf functionality.
 */
class MenuLeaf {

  /**
   * Vocabulary ID.
   *
   * @var mixed|null
   */
  private $vid = NULL;

  /**
   * Tag ID.
   *
   * @var mixed|null
   */
  public $tid = NULL;

  /**
   * Term name.
   *
   * @var mixed|null
   */
  public $termName = NULL;

  /**
   * Url alias.
   *
   * @var mixed|null
   */
  public $urlAlias = NULL;

  /**
   * Leaf class.
   *
   * @var mixed|null
   */
  public $leafClass = NULL;

  /**
   * Link url.
   *
   * @var null
   */
  public $linkUrl = NULL;

  /**
   * Leaf anchor class.
   *
   * @var mixed|null
   */
  public $leafAnchorClass = NULL;

  /**
   * Filters object.
   *
   * @var array|\Drupal\taxonomy_facets\SelectedFilters|mixed|null
   */
  private $filtersObject = NULL;

  /**
   * Construct of the MenuLeaf class.
   */
  public function __construct($leaf_term_object, $leafClass = []) {

    // Get fully loaded terms for all applied filters.
    $this->filtersObject = taxonomy_facets_get_selected_filters();
    $this->termName = $leaf_term_object->name;
    $this->tid = $leaf_term_object->tid;
    $this->vid = $leaf_term_object->vid;
    if ($leafClass) {
      $this->leafAnchorClass = $leafClass['leafAnchorClass'];
      $this->leafClass = $leafClass['leafClass'];
    }
    $this->getTermUrlAlias();
    $this->buildLinkUrl();
  }

  /**
   * Get taxonomy term url alias from the term object.
   */
  private function getTermUrlAlias() {
    $query = \Drupal::database()->select('path_alias', 'p')
      ->fields('p', ['alias'])
      ->condition('path', '/taxonomy/term/' . $this->tid)
      ->execute();
    if ($aliases = $query->fetchAll()) {
      $aliases = current($aliases)->alias;
      $this->urlAlias = ltrim($aliases, "/");
    }
    else {
      \Drupal::messenger()->addWarning(t('You do not have any url aliases generate for taxonomy terms, see Taxonomy Facets module readme file.'));
    }
  }

  /**
   * Builds link URL.
   */
  private function buildLinkUrl() {
    $url = [];
    $noTermFromCurrentVocabularyFound = TRUE;

    $filters = NULL;
    if ($this->filtersObject) {
      $filters = $this->filtersObject->getAppliedFilters();
    }

    if ($filters) {
      // Loop trough applied filters.
      foreach ($filters as $filter) {
        // If filter is from current vocabulary than apply this leaf url alias
        // instead of already applied filter.
        if ($filter->bundle() == $this->vid) {
          $obj = new \stdClass();
          $obj->vid = $this->vid;
          $obj->url = $this->urlAlias;
          $url[] = $obj;
          $noTermFromCurrentVocabularyFound = FALSE;
        }
        // Put in url alias of the applied filter.
        else {
          $obj = new \stdClass();
          $obj->vid = $filter->bundle();
          $obj->url = ltrim($filter->url(), "/");
          $url[] = $obj;
        }
      }
    }

    // If filters from this vocabulary were not in the applied filters than
    // also apply the alias from the current leaf.
    if ($noTermFromCurrentVocabularyFound == TRUE) {
      $obj = new \stdClass();
      $obj->vid = $this->vid;
      $obj->url = $this->urlAlias;
      $url[] = $obj;
    }
    // @todo replace 'listings' hard coded string with user configurable variable.
    $this->linkUrl = $this->getLanguagePrefix() . '/listings';

    // Now order url aliases (filters) by vocabulary id so that we preserve
    // order, so we don't end up with duplicate pages for same filter
    // combinations.
    usort($url, function ($a, $b) {
      if ($a->vid == $b->vid) {
        return 0;
      }
      return ($a->vid < $b->vid) ? -1 : 1;
    });

    foreach ($url as $u) {
      $this->linkUrl .= '/' . $u->url;
    }
  }

  /**
   * Gets the language prefix.
   */
  public function getLanguagePrefix() {
    if ($prefixes = \Drupal::config('language.negotiation')
      ->get('url.prefixes')) {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($prefixes[$language]) {
        return "/" . $prefixes[$language];
      }
    }
    // @todo add case when using different domains for language negotiation.
    return NULL;
  }

}
