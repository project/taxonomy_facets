<?php

namespace Drupal\taxonomy_facets\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Taxonomy Facets Menu' block.
 *
 * @Block(
 *   id = "taxonomy_facets_block1",
 *   admin_label = @Translation("Taxonomy Facets block 1"),
 * )
 */
class TaxonomyFacetsBlock1 extends TaxonomyFacetsBaseBlock {
}
