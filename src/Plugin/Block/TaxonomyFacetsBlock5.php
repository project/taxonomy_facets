<?php

namespace Drupal\taxonomy_facets\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Taxonomy Facets Menu' block.
 *
 * @Block(
 *   id = "taxonomy_facets_block5",
 *   admin_label = @Translation("Taxonomy Facets block 5"),
 * )
 */
class TaxonomyFacetsBlock5 extends TaxonomyFacetsBaseBlock {
}
