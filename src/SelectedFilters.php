<?php

namespace Drupal\taxonomy_facets;

use Drupal\taxonomy\Entity\Term;

/**
 * Class devoted to work with selected filters.
 */
class SelectedFilters {

  /**
   * Terms, array of term objects.
   *
   * @var array
   */
  protected $terms = [];

  /**
   * Construct of the SelectedFilters class.
   */
  public function __construct($term_names = []) {
    // Set filters.
    foreach ($term_names as $term_name) {
      if ($tid = self::getTermIdFromUrlAlias($term_name)) {
        $this->terms[] = Term::load($tid);
      }
    }

    // Sort by vocabulary id so that we always get same order of filters,
    // to avoid duplicate urls for the same page.
    if ($this->terms) {
      usort($this->terms, function ($a, $b) {
        if ((int) $a->id() == (int) $b->id()) {
          return 0;
        }

        if ((int) $a->id() < (int) $b->id()) {
          return -1;
        }
        else {
          return 1;
        }
      });
    }
  }

  /**
   * Get term id.
   *
   * For a given taxonomy term name return the term id.
   *
   * @return int
   *   Return the term id. return null if no term with this name found.
   *
   * @todo deal with duplicate term names, i.e same name in 2 vocabularies.
   */
  public static function getTermIdFromUrlAlias($term_alias) {

    $select = \Drupal::database()->select('path_alias', 'p');

    // Select these specific fields for the output.
    $select->addField('p', 'path');

    // Filter only persons named "John".
    $select->condition('p.alias', '/' . $term_alias);

    $entries = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($entries)) {
      $entries = explode('/', current(current($entries)));
      return $entries[3];
    }
    else {
      return NULL;
    }
  }

  /**
   * Gets applied filters.
   */
  public function getAppliedFilters() {
    return $this->terms;
  }

  /**
   * Gets applited filter tids.
   */
  public function getAppliedFilterTids() {
    $tids = [];
    foreach ($this->terms as $term) {
      $tids[] = $term->id();
    }
    return $tids;
  }

  /**
   * Get the term id of the filter that belongs to a given vocabulary.
   */
  public function getSelectedFilterForVocabulary($vid) {
    foreach ($this->terms as $term) {
      if ($term->bundle() == $vid) {
        return $term->id();
      }
    }
    return NULL;
  }

  /**
   * Gets selected filter term for vocabulary.
   */
  public function getSelectedFilterTermForVocabulary($vid) {
    foreach ($this->terms as $term) {
      if ($term->bundle() == $vid) {
        return $term;
      }
    }
    return NULL;
  }

}
